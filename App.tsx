import React from 'react';
import Home from './components/home';
import { NativeRouter, Switch, Route } from 'react-router-native';
import Espace from './components/espace';
import { View, AsyncStorage } from 'react-native';
import Configuration from './components/configuration';
import CreateIndicator from './components/createIndicator';
import UpdateIndicator from './components/updateIndicator';
import Graphic from './components/graphic';
import Profile from './components/profil';
import Login from './components/login';
import Register from './components/register';

export default function App() {
  //AsyncStorage.removeItem('User');
  return (
    <NativeRouter>
      <View>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/espace" component={Espace} />
          <Route exact path="/configuration" component={Configuration} />
          <Route exact path="/create/indicator" component={CreateIndicator} />
          <Route exact path="/update/indicator" component={UpdateIndicator}/>
          <Route exact path="/graphic" component={Graphic}/>
          <Route exact path="/profile" component={Profile}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/register" component={Register}/>
        </Switch>
      </View>
    </NativeRouter>
  );
}