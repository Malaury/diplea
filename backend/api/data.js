const express = require('express');
const db = require('../utils/mysqldb');
const router = express.Router();

router.post('/data/:id', (req, res) => {
    if(! req.body.data){
        res.status(404);
        return;
    }
    const queryString = "DELETE FROM data WHERE id_user=?";
    db.connection.query(queryString, [req.params.id], (err, rows) => {
        if (err) throw err;
    });
    for(let cpt=0;cpt< req.body.data.length;cpt++){
        const d = req.body.data[cpt];
        const queryString = "INSERT INTO data (\`id_data\`,\`id_indicator\`,\`data_text\`,\`tracker\`,\`data_date\`,\`id_user\`) VALUES (?,?,?,?,?,?)";
        db.connection.query(queryString, [d.id_data, d.id_indicator,d.data_text,d.tracker, d.data_date,req.params.id], (err, rows) => {
            if (err) throw err;
        });
    }
    res.send("0");
});

router.get('/data/:id', (req, res) => {
    const queryString = "SELECT * FROM data WHERE id_user=?";
    db.connection.query(queryString, [req.params.id], (err, rows) => {
        if (err) throw err;
        res.json(rows);
    });
});

router.post('/indicator/:id', (req, res) => {
    if(! req.body.data) {
        res.status(404);
        return;
    };
    const queryString = "DELETE FROM indicator WHERE id_user=?";
    db.connection.query(queryString, [req.params.id], (err, rows) => {
        if (err) throw err;
    });
    for(let cpt=0;cpt< req.body.data.length;cpt++){
        const d = req.body.data[cpt];
        const queryString = "INSERT INTO indicator (\`id_espace\`,\`id_indicator\`,\`display_order\`,\`type\`,\`hidden\`,\`title\`,\`id_user\`) VALUES (?,?,?,?,?,?,?)";
        db.connection.query(queryString, [d.id_espace, d.id_indicator,d.display_order,d.type, d.hidden,d.title,req.params.id], (err, rows) => {
            if (err) throw err;
        });
    }
    res.send("0");
});

router.get('/indicator/:id', (req, res) => {
    const queryString = "SELECT * FROM indicator WHERE id_user=?";
    db.connection.query(queryString, [req.params.id], (err, rows) => {
        if (err) throw err;
        res.json(rows);
    });
});

router.post('/selector/:id', (req, res) => {
    if(! req.body.data) {
        res.status(404);
        return;
    }
    const queryString = "DELETE FROM selector WHERE id_user=?";
    db.connection.query(queryString, [req.params.id], (err, rows) => {
        if (err) throw err;
    });
    for(let cpt=0;cpt< req.body.data.length;cpt++){
        const d = req.body.data[cpt];
        const queryString = "INSERT INTO selector (\`id_selector\`,\`id_indicator\`,\`name\`,\`id_user\`) VALUES (?,?,?,?)";
        db.connection.query(queryString, [d.id_selector, d.id_indicator,d.name,req.params.id], (err, rows) => {
            if (err) throw err;
        });
    }
    res.send("0");
});

router.get('/selector/:id', (req, res) => {
    const queryString = "SELECT * FROM selector WHERE id_user=?";
    db.connection.query(queryString, [req.params.id], (err, rows) => {
        if (err) throw err;
        res.json(rows);
    });
});

router.post('/espace/:id', (req, res) => {
    if(!req.body.data) {
        res.status(404);
        return;
    }
    const queryString = "DELETE FROM espace WHERE id_user=?";
    db.connection.query(queryString, [req.params.id], (err, rows) => {
        if (err) throw err;
    });
    for(let cpt=0;cpt< req.body.data.length;cpt++){
        const d = JSON.parse(req.body.data[cpt]);
        const queryString = "INSERT INTO espace (\`id_espace\`,\`name\`,\`id_user\`) VALUES (?,?,?)";
        db.connection.query(queryString, [d.id_espace, d.name ,req.params.id], (err, rows) => {
            if (err) throw err;
        });
    }
    res.send("0");
});

router.get('/espace/:id', (req, res) => {
    const queryString = "SELECT * FROM espace WHERE id_user=?";
    db.connection.query(queryString, [req.params.id], (err, rows) => {
        if (err) throw err;
        res.json(rows);
    });
});

module.exports = router;