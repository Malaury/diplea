const express = require('express');
const db = require('../utils/mysqldb');
const router = express.Router();

router.get('/:username', (req, res) => {
    const queryString = "SELECT * FROM user where username=?";
    db.connection.query(queryString, [req.params.username], (err, rows) => {
        if (err) throw err;
        res.json(rows);
    });
});

router.post('', (req, res) => {
    const queryString = "INSERT INTO user (\`username\`,\`password\`) VALUES (?,?)";
    db.connection.query(queryString, [req.body.username, req.body.password], (err, rows) => {
        if (err) throw err;
        res.json(rows);
    });
});

router.post('/check', (req, res) => {
    if(!req.body.username || !req.body.password) res.status(404);
    const queryString = "SELECT * FROM user where username=? AND password=?";
    db.connection.query(queryString, [req.body.username, req.body.password], (err, rows) => {
        if (err) throw err;
        res.json(rows);
    });
});

module.exports = router;