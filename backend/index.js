const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(express.json());

// Body Parser configuration
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

const data = require('./api/data');
app.use('/api/data', data);

const users = require('./api/users');
app.use('/api/user', users)

app.listen(3000, () => console.log('Listenning on port 3000 ...'));

