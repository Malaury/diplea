import React from "react";
function parseData(data){
    const result = [];
    for(const d of data){
        result.push(JSON.parse(d))
    }
    return result;
}
export  {parseData};