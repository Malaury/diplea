import * as SQLite from 'expo-sqlite';
import { AsyncStorage, DatePickerAndroid } from 'react-native';
import { removeDataByIdIndicator } from './data';
import { deleteSelectorByIndicator } from './selector';
import {parseData} from './utils';

interface Indicator {
    id_user: number,
    id_indicator: number,
    title: string,
    type: string,
    display_order: number,
    hidden: string,
    id_espace: number,
}
async function putIndicator(id_espace: string, title: string, type: string, display_order: string, hidden: string) {
    const indicator = await getIndicatorByTitleAndEspaceId(id_espace,title);
    if(indicator.length !== 0) return false;
    
    const indicators = await getIndicators();
    let newId= 0;
    for(const indicator of indicators){
        if(parseInt(indicator.id_indicator) >= newId){
            newId = parseInt(indicator.id_indicator) + 1;
        }
    }
    await putIndicatorFromDB(id_espace,title,type,display_order,hidden,newId+'');
    return true;
}

async function deleteIndicatorByEspace(id_espace: string){
    const indicators = await getIndicators();
    for(const indicator of indicators){
        if (indicator.id_espace === id_espace) {
            AsyncStorage.removeItem(`Indicator/${indicator.id_indicator}`);
            removeDataByIdIndicator(indicator.id_indicator);
            deleteSelectorByIndicator(indicator.id_indicator);
            console.log("Delete an indicator");
        }
    }
}

async function getIndicatorsByEspace(id_espace: string) {
    const indicators = await getIndicators();
    const result = [];
    for(const indicator of indicators){
        if(indicator.hidden === "Visible" && parseInt(indicator.id_espace) === parseInt(id_espace)){
            result.push(indicator);
        }
    }
    return result;
}

async function getAllIndicatorsByEspace(id_espace: string) {
    const indicators = await getIndicators();
    const result = [];
    for(const indicator of indicators){
        if(indicator.id_espace === id_espace){
            result.push(indicator);
        }
    }
    return result;
}

async function getIndicatorByTitleAndEspaceId(title: string, id_espace: string) {
    const indicators = await getIndicators();
    for(const indicator of indicators){
        if(indicator.title === title && indicator.id_espace === id_espace){
            return [indicator];
        }
    }
    return [];
}
async function getIndicators() {
    const keys = await AsyncStorage.getAllKeys();
    const indicators = [];
    for(const key of keys){
        if (key.search("Indicator") !== -1) {
            indicators.push(await AsyncStorage.getItem(key));
        }
    }
    const tab = parseData(indicators);
    tab.sort(function compare(a, b) {
        if (parseInt(a.display_order) < parseInt(b.display_order))
           return -1;
        if (parseInt(a.display_order)> parseInt(b.display_order) )
           return 1;
        return 0;
      });
    return tab;
}
async function updateIndicator(id_espace: string, title: string, display_order: string, hidden: string, type: string, id_indicator: string){
    if(parseInt(display_order)=== NaN) return false;
    await deleteIndicatorByTitleAndEspace(title,id_espace);
    await putIndicatorFromDB(id_espace, title, type, display_order, hidden, id_indicator);
    return true;
}

async function deleteIndicatorByTitleAndEspace(title: string,id_espace: string){
    const indicators = await getIndicators();
    for(const indicator of indicators){
        if (indicator.title === title && indicator.id_espace === id_espace) {
            AsyncStorage.removeItem(`Indicator/${indicator.id_indicator}`);
            removeDataByIdIndicator(indicator.id_indicator);
            deleteSelectorByIndicator(indicator.id_indicator);
            console.log("Delete an indicator");
            return true;
        }
    }
    return false;
}

function putIndicatorFromDB(id_espace: string, title: string, type: string, display_order: string, hidden: string, id_indicator: string){
    AsyncStorage.setItem(`Indicator/${id_indicator}`, `{
        "id_espace": "${id_espace}",
        "title": "${title}",
        "type": "${type}",
        "display_order": "${display_order}",
        "hidden": "${hidden}",
        "id_indicator": "${id_indicator}"
    }`);
    console.log("Set indicator " + title);
}

async function truncateIndicator(){
    const keys = await AsyncStorage.getAllKeys();
    keys.forEach(key => {
        if (key.search("Indicator") !== -1) {
            AsyncStorage.removeItem(key);
            console.log("Delete an indicator");
        }
    })
 }
export { putIndicator,deleteIndicatorByTitleAndEspace, getAllIndicatorsByEspace,getIndicatorsByEspace, deleteIndicatorByEspace, getIndicatorByTitleAndEspaceId, updateIndicator, getIndicators, truncateIndicator, putIndicatorFromDB }