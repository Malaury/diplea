import * as SQLite from 'expo-sqlite';
import { AsyncStorage } from 'react-native';
import {parseData} from './utils';

const db = SQLite.openDatabase('diplea.db');

interface Selector {
    id_selector: number,
    id_indicator: number,
    name: string
}

async function putSelector(id_indicator: number, name: string) {
    console.log("put selector")
    const selectors = await getSelectors();
    let newId= 0;
    for(const selector of selectors){
        if(parseInt(selector.id_selector) >= newId){
            newId = parseInt(selector.id_selector) + 1;
        }
    }
    await putSelectorFromDB(id_indicator,name,newId);
    return true;
}

async function deleteSelectorByIndicator(id_indicator: string){
    const selectors = await getSelectors();
    for(const selector of selectors){
        if (selector.id_indicator === id_indicator) {
            AsyncStorage.removeItem(`Selector/${selector.id_selector}`);
            console.log("Delete an Selector");
        }
    }
}

async function getSelectorsByIndicator(id_indicator: string) {
    const selectors = await getSelectors();
    const result = [];
    for(const selector of selectors){
        if (selector.id_indicator === id_indicator) {
            result.push(selector);
        }
    }
    return result;
}

async function getSelectors() {
    const keys = await AsyncStorage.getAllKeys();
    const selectors = [];
    for(const key of keys){
        if (key.search("Selector") !== -1) {
            selectors.push(await AsyncStorage.getItem(key));
        }
    }
    return parseData(selectors);
}

function putSelectorFromDB(id_indicator: number, name: string, id_selector: number){
    AsyncStorage.setItem(`Selector/${id_selector}`, `{
        "id_indicator": "${id_indicator}",
        "name": "${name}",
        "id_selector": "${id_selector}"
    }`);
    console.log("Set selector " + id_selector);
}

async function truncateSelector(){
    const keys = await AsyncStorage.getAllKeys();
    keys.forEach(key => {
        if (key.search("Selector") !== -1) {
            AsyncStorage.removeItem(key);
            console.log("Delete an selector");
        }
    })
 }

 async function deleteSelectorValue(id_indicator: string, name: string){
     const selectors = await getSelectorsByIndicator(id_indicator);
     for(const selector of selectors ){
         if(selector.name === name){
             await AsyncStorage.removeItem(`Selector/${selector.id_selector}`);
             return;
         }
     }
 }
export { putSelector, getSelectorsByIndicator, getSelectors, truncateSelector,putSelectorFromDB,deleteSelectorByIndicator, deleteSelectorValue }