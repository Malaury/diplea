import * as SQLite from 'expo-sqlite';
import { AsyncStorage } from 'react-native';
import { deleteIndicatorByEspace } from './indicateur';
import {parseData} from './utils';
interface Espace {
    id_espace: number,
    name: string
}
async function putEspace(name: string) {
        const newEspace = await getEspaceByName(name);
        if(newEspace.length > 0) return false;
        const espaces = await getEspaces();
        let newId= 0;
        for(const espace of espaces){
            if(parseInt(espace.id_espace) >= newId){
                newId = parseInt(espace.id_espace) + 1;
            }
        }
        await AsyncStorage.setItem(`Espace/${newId}`, `{"id_espace": "${newId}", "name": "${name}"}`);
        return true;
}

async function deleteEspaceByName(name: string){
    const espace = await getEspaceByName(name);
    if(espace.length === 0) return false;
    await AsyncStorage.removeItem(`Espace/${espace[0].id_espace}`);
    deleteIndicatorByEspace(espace[0].id_espace);
    console.log("Delete an espace");
    return true;
}

function putEspaceFromDB(name: string, id_espace: number) {
    AsyncStorage.setItem(`Espace/${id_espace}`, `{"name": "${name}", "id_espace": "${id_espace}"}`);
    console.log("Set espace " + name)
}

async function getEspaceByName(name: string){
    const espaces = parseData(await getEspaces());
    for(const espace of espaces){
        if(espace.name === name) return [espace];
    }
    return[];
}

async function getEspaces() {
    const keys = await AsyncStorage.getAllKeys();
    const espaces = [];
    for (const key of keys){
        if (key.search("Espace") !== -1) {
            const es = await AsyncStorage.getItem(key);
            espaces.push(es);
        }
    } 
    return espaces;
}

async function truncateEspace() {
    const keys = await AsyncStorage.getAllKeys();
    keys.forEach(key => {
        if (key.search("Espace") !== -1) {
            AsyncStorage.removeItem(key);
            console.log("Delete an espace");
        }
    })
}

export { getEspaces, putEspace, deleteEspaceByName, getEspaceByName, truncateEspace, putEspaceFromDB }
