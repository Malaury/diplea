import * as SQLite from 'expo-sqlite';
import { AsyncStorage } from 'react-native';
import {parseData} from './utils';

const db = SQLite.openDatabase('diplea.db');

interface Data {
    id_data: number,
    id_indicator: number,
    data_text: string,
    data_date: Date,
    tracker: boolean,
    selector: string
}

async function putData(id_indicator: string, data_text: string, data_date: string, tracker: boolean) {
    const data = await getData();
    let newId= 0;
    for(const d of data){
        if(parseInt(d.id_data) >= newId){
            newId = parseInt(d.id_data) + 1;
        }
    }
    await putDataFromDB(id_indicator,data_text,data_date,tracker,newId);
    return true;
}

async function removeDataByIdIndicator(id_indicator: string){
    const data = await getData();
    for(const d of data){
        if (d.id_indicator === id_indicator) {
            AsyncStorage.removeItem(`Data/${d.id_data}`);
            console.log("Delete one data");
        }
    }
}

async function updateData(id_indicator: string, data_text: string, tracker: boolean, data_date: string) {
    const data = await getDataByDateAndIndicator(data_date,id_indicator);
    if( data.length === 0) return;
    await removeDataByIdIndicatorAndDate(id_indicator,data_date);
    await putDataFromDB(id_indicator, data_text, data_date, tracker,data[0].id_data);
}

async function removeDataByIdIndicatorAndDate(id_indicator: string, data_date: string){
    await AsyncStorage.removeItem(`Data/${id_indicator}/${data_date}`);
    console.log("Delete an data");
    return true;
}

async function getDataByDateAndIndicator(data_date: string, id_indicator: string) {
    const data = await getData();
    for(const d of data){
        if (d.data_date === data_date && d.id_indicator === id_indicator) {
            return [d]
        }
    }
    return [];
}

async function getData() {
    const keys = await AsyncStorage.getAllKeys();
    const data = [];
    for(const key of keys){
        if (key.search("Data") !== -1) {
            data.push( await AsyncStorage.getItem(key));
        }
    }
    return parseData(data);
}

async function truncateData(){
    const keys = await AsyncStorage.getAllKeys();
    keys.forEach(key => {
        if (key.search("Data") !== -1) {
            AsyncStorage.removeItem(key);
            console.log("Delete an data");
        }
    })
}

function putDataFromDB(id_indicator: string, data_text: string, data_date: string, tracker: boolean, id_data: number){
    AsyncStorage.setItem(`Data/${id_indicator}/${data_date}`, `{
        "id_indicator": "${id_indicator}",
        "data_text": "${data_text}",
        "data_date": "${data_date}",
        "tracker": "${tracker}",
        "id_data": "${id_data}"
    }`);
    console.log("Set data " + id_data);
}
export { putData, getDataByDateAndIndicator, updateData, getData, truncateData, putDataFromDB, removeDataByIdIndicator };