import * as SQLite from 'expo-sqlite';
import { AsyncStorage } from 'react-native';

const db = SQLite.openDatabase('diplea.db');

interface User{
    id_user: number,
    username: string
}
async function deleteUser(){
    await AsyncStorage.removeItem('User');
}
async function putUser(id_user: number){
    await AsyncStorage.setItem('User',`${id_user}`);
    console.log("put user")
}

async function getUserLoged(){
    return await AsyncStorage.getItem('User')
}
export {getUserLoged, putUser, deleteUser};