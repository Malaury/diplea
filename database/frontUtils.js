import * as axios from 'axios';

var instance = axios.create();
instance.defaults.baseURL = 'http://192.168.1.74:3000/api';
instance.defaults.timeout = 20000;
//...
//and other options

export { instance as default };