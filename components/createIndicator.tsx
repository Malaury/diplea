import React, { useState, useEffect } from 'react';
import { View, Text, Picker, TouchableOpacity, TextInput, AsyncStorage } from 'react-native';
import { Redirect } from 'react-router-dom';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Formik } from 'formik';
import { putIndicator } from '../database/indicateur';

let CreateIndicator = () => {

    const [back, setBack] = useState(false);
    const [idEspace, setIdEspace] = useState(null);
    const [titleUniq, setTitleUniq] = useState(true);
    const [orderNotInt, setOrderNotInt] = useState(false);

    useEffect(() => {
        AsyncStorage.getItem('id_espace').then(data => {
            setIdEspace(data);
        })
    }, []);

    return (
        <View>
            {back === true && <Redirect to='/configuration' />}
            <TouchableOpacity style={{ backgroundColor: 'black', height: hp('7%') }}></TouchableOpacity>
            <TouchableOpacity style={{ backgroundColor: '#CD5C5C', height: hp('6%'), marginTop: hp('5%') }}>
                <Text style={{ marginTop: hp('1.5%'), textAlign: 'center', fontSize: 17, color: 'white' }}>Creation d'un nouvel indicateur</Text>
            </TouchableOpacity>

            <Formik
                initialValues={{
                    title: '',
                    type: 'Text',
                    hidden: 'Visible',
                    order: '0'
                }}
                onSubmit={values => {
                    if (values.title === '') return;
                    if (values.order === '') values.order = '0';
                    console.log(values);
                    putIndicator(idEspace, values.title, values.type, values.order, values.hidden).then((result: boolean) => {
                        if (result === true){
                           setBack(true);
                           setTitleUniq(true); 
                        } 
                        else if(result===false){
                            setTitleUniq(false);
                            setOrderNotInt(false);
                        }
                        else{
                            setOrderNotInt(true);
                        }
                    })
                }}
            >
                {({ handleChange, handleBlur, handleSubmit, values }) => (
                    <View style={{ alignItems: 'center' }}>
                        <View style={orderNotInt ?{ height: hp('55%'), width: wp('90%'), backgroundColor: 'pink', marginTop: hp('8%'), borderRadius: 10 }:{ height: hp('53%'), width: wp('90%'), backgroundColor: 'pink', marginTop: hp('8%'), borderRadius: 10 }}>
                            <View style={{ flexDirection: "row", marginTop: hp('7%') }}>
                                <Text style={{ color: 'black', fontSize: 15, marginLeft: wp('10%') }}>Titre:</Text>
                                <TextInput onChangeText={handleChange('title')} onBlur={handleBlur('title')}
                                    value={values.title} style={{ backgroundColor: 'white', width: wp('26%'), height: hp('3%'), borderColor: 'black', borderStyle: 'solid', borderWidth: 1, marginLeft: wp('2%') }}></TextInput>
                            </View>
                            {!titleUniq && <Text style={{ marginTop: hp('1%'), marginLeft: wp('10%'), color: 'red' }}>Ce nom d'indicateur est déjà prit.</Text>}
                            <View style={!titleUniq && orderNotInt ?{ flexDirection: "row", marginTop: hp('1%') }:  { flexDirection: "row", marginTop: hp('5%') }}>
                                <Text style={{ marginLeft: wp('10%'), fontSize: 15 }}>Type d'indicateur:</Text>
                                <Picker onValueChange={handleChange('type')} selectedValue={values.type} style={{ height: hp('3%'), width: wp('30%') }} >
                                    <Picker.Item label='Text' value='Text' />
                                    <Picker.Item label='Tracker' value='Tracker' />
                                    <Picker.Item label='Select' value='Select' />
                                    <Picker.Item label='Timeur' value='Timeur' />
                                </Picker>
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                {values.type === "Select" && <Text style={{ width: wp('70%'), marginTop: hp('2%'), fontSize: 10, color: 'green' }}> Pour remplir votre indicateur selecteur, rendez vous dans la catégorie "mise à jour" de votre indicateur.</Text>}
                            </View>

                            <View style={values.type === "Select" ? { flexDirection: "row", marginTop: hp('3%') } : { flexDirection: "row", marginTop: hp('6%') }}>
                                <Text style={{ marginLeft: wp('10%') }}>Caché:</Text>
                                <Picker onValueChange={handleChange('hidden')} selectedValue={values.hidden} style={{ height: hp('3%'), width: wp('32%') }} >
                                    <Picker.Item label='Visible' value='Visible' />
                                    <Picker.Item label='Caché' value='Caché' />
                                </Picker>
                            </View>

                            <View style={{ flexDirection: "row", marginTop: hp('5%') }}>
                                <Text style={{ marginLeft: wp('10%') }}>Ordonnancement:</Text>
                                <TextInput onChangeText={handleChange('order')} onBlur={handleBlur('order')}
                                    value={values.order} style={{ backgroundColor: 'white', width: wp('30%'), height: hp('3%'), marginLeft: wp('1%'), borderColor: 'black', borderStyle: 'solid', borderWidth: 1 }}></TextInput>
                            </View>
                            {orderNotInt && <Text style={{ color: 'red', marginLeft: wp('10%'), marginTop: hp('2%') }}>L'Ordonnancement doit être un nombre.</Text>}
                            <TouchableOpacity style={{ backgroundColor: 'black', marginTop: hp('5%') }} onPress={() => handleSubmit()}>
                                <Text style={{ textAlign: 'right', marginRight: wp('7%'), color: 'white', marginBottom: 2, marginTop: 1 }}>Enregistrer</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ backgroundColor: '#CD5C5C', marginTop: 5 }} onPress={() => {
                                setBack(true);
                            }}>
                                <Text style={{ textAlign: 'right', marginRight: wp('10%'), color: 'white', marginBottom: 2, marginTop: 1 }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )}
            </Formik>
        </View>
    );
}
export default CreateIndicator;