import React, { useState } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Redirect } from 'react-router-dom';
import { Formik } from 'formik';
import instance from '../database/frontUtils';

const Register = () => {
    const [redirectToLogin, setRedirectToLogin] = useState(false);
    const [usernameAlreadyUsed, setUsernameAlreadyUsed] = useState(false);

    return (
        <View style={{ height: hp('100%') }}>
            {redirectToLogin && <Redirect to='/login' />}
            <TouchableOpacity disabled={true} style={{ backgroundColor: '#CD5C5C', height: hp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ color: 'white', fontSize: 20, position: 'absolute', bottom: 5 }}>Création de compte</Text>
            </TouchableOpacity>
            <Formik
                initialValues={{
                    'username': '',
                    'password': ''
                }}
                onSubmit={values => {
                    if(values.username==="" || values.password=== "") return;
                    instance.get(`/user/${values.username}`, {
                    }).then((res)=> {
                            if (res.data.length !== 0) {
                                setUsernameAlreadyUsed(true);
                                return;
                            }
                            else {
                                setUsernameAlreadyUsed(false);
                                instance.post(`/user`, {'username': values.username, 'password': values.password}, {},{
                                }).then( setRedirectToLogin(true))
                                    .catch((error) =>{console.log(error)});
                            }
                        })
                        .catch((error) =>{console.log(error)});
                }}
            >
                {({ handleChange, handleSubmit, values }) => (
                    <View style={{ alignItems: 'center' }}>
                        <View style={{ height: hp('25%'), width: wp('90%'), backgroundColor: '#FEEBBD', marginTop: hp('7%'), borderRadius: 10 }}>
                            <View style={{ flexDirection: 'row', marginTop: hp('4%'), marginLeft: wp('15%') }}>
                                <Text style={{ fontSize: 18 }}>Pseudo: </Text>
                                <TextInput onChangeText={handleChange('username')} value={values.username} style={{ height: hp('3%'), width: wp('30%'), backgroundColor: 'white', marginTop: 4, marginLeft: wp('2%'), borderWidth: 1 }}></TextInput>
                            </View>
                            {usernameAlreadyUsed && <Text style={{marginLeft: wp('15%'), marginTop: hp('1%')}}>Pseudo déjà utilisé.</Text>}
                            <View style={ usernameAlreadyUsed?{ flexDirection: 'row', marginTop: hp('1%'), marginLeft: wp('15%') }:{ flexDirection: 'row', marginTop: hp('3%'), marginLeft: wp('15%') }}>
                                <Text style={{ fontSize: 18 }}>Mot de passe: </Text>
                                <TextInput onChangeText={handleChange('password')} value={values.password} style={{ height: hp('3%'), width: wp('30%'), backgroundColor: 'white', marginTop: 4, marginLeft: wp('2%'), borderWidth: 1 }}></TextInput>
                            </View>
                            <View>
                                <TouchableOpacity onPress={()=>{handleSubmit()}}style={ usernameAlreadyUsed? { backgroundColor: '#CD5C5C', width: wp('31%'), height: hp('4%'), right: 0, marginTop: hp('3%'), marginLeft: ('61%'), justifyContent: 'center' }: { backgroundColor: '#CD5C5C', width: wp('31%'), height: hp('4%'), right: 0, marginTop: hp('5%'), marginLeft: ('61%'), justifyContent: 'center' }}>
                                    <Text style={{ color: 'white', textAlign: 'center' }}>S'enregistrer</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ alignItems: 'center', marginTop: hp('7%') }}>
                            <Image style={{
                                width: wp('60%'),
                                height: hp('30%')
                            }} source={require("../assets/factory.png")}>
                            </Image>
                        </View>
                    </View>
                )}
            </Formik>
            <TouchableOpacity style={{ width: wp('40%'), height: hp('20%'), bottom: hp('-5%'), position: 'absolute', justifyContent: 'center' }} onPress={() => setRedirectToLogin(true)}>
                <Image style={{
                    width: wp('15%'),
                    height: hp('7%'), marginLeft: wp('10%')
                }} source={require("../assets/left-arrow.png")} >
                </Image>
            </TouchableOpacity>
        </View >
    );
}
export default Register;