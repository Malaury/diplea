import React, { useState, useEffect } from 'react';
import { View, Text, Picker, TouchableOpacity, TextInput, CheckBox, AsyncStorage, Modal, TouchableHighlight } from 'react-native';
import { Redirect } from 'react-router-dom';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Formik } from 'formik';
import { updateIndicator, getIndicatorByTitleAndEspaceId } from '../database/indicateur';
import { getSelectorsByIndicator, putSelector, deleteSelectorValue } from '../database/selector';

const UpdateIndicator = () => {

    const [back, setBack] = useState(false);
    const [idEspace, setIdEspace] = useState(null);
    const [indicatorTitle, setIndicatorTitle] = useState(null);
    const [modalIndicatorTitleVisible, setModalIndicatorTitleVisible] = useState(true);
    const [indicatorSelected, setIndicatorSelected] = useState(null);
    const [selector, setSelector] = useState(null);
    const [initialValue, setInitialValue] = useState({
        title: "",
        hidden: '',
        order: "0",
        type: ""
    });
    const [newSelectorInput, setNewSelectorInput] = useState("");
    const [indicatorDoesNotExist, setIndicatorDoesNotExist] = useState(false);
    const [orderNotInt, setOrderNotInt] = useState(false);
    const [deleteSelectorInput, setDeleteSelectorInput]= useState("");

    useEffect(() => {
        AsyncStorage.getItem('id_espace').then(data => {
            setIdEspace(data);
        });
    }, []);

    useEffect(() => {
        if (!indicatorSelected) return;
        setInitialValue({
            title: indicatorSelected.title,
            hidden: indicatorSelected.hidden,
            order: "" + indicatorSelected.display_order,
            type: indicatorSelected.type
        });
        if (indicatorSelected.type === 'Select') {
            getSelectorsByIndicator(indicatorSelected.id_indicator).then(s => {
                setSelector(s);
            })
        }
    }, [indicatorSelected]);

    useEffect(() => {
        if (indicatorSelected && indicatorSelected.type === 'Select') {
            getSelectorsByIndicator(indicatorSelected.id_indicator).then(s => {
                setSelector(s);
            })
        }
    }, [newSelectorInput, deleteSelectorInput]);

    function checkIndicatorTitle() {
        getIndicatorByTitleAndEspaceId(indicatorTitle, idEspace).then(indicator => {
            if (indicator.length !== 0) {
                setIndicatorSelected(indicator[0]);
                setModalIndicatorTitleVisible(false);
                setIndicatorTitle("");
                setIndicatorDoesNotExist(false);
            }else{
                setIndicatorDoesNotExist(true);
            }
        })
    }

    return (
        <View>
            {back === true && <Redirect to='/configuration' />}
            <TouchableOpacity style={{ backgroundColor: 'black', height: hp('7%') }}></TouchableOpacity>
            <TouchableOpacity style={{ backgroundColor: '#CD5C5C', height: hp('6%'), marginTop: hp('5%') }}>
                <Text style={{ marginTop: hp('1.5%'), textAlign: 'center', fontSize: 17, color: 'white' }}>Mise à jour d'un indicateur</Text>
            </TouchableOpacity>

            <Formik
                enableReinitialize={true}
                initialValues={initialValue}
                onSubmit={values => {
                    if (values.title === '') return;
                    if (values.order === '') values.order = '0';
                    updateIndicator(idEspace,values.title, values.order, values.hidden, indicatorSelected.type ,indicatorSelected.id_indicator).then((result:boolean) => {
                        if(result===false){
                            setOrderNotInt(true);
                        }
                        else{
                             setBack(true);
                        }
                    })
                }}
            >
                {({ handleChange, handleBlur, handleSubmit, values }) => (
                    <View style={{ alignItems: 'center' }}>
                        <View style={values.type === "Select" ? { height: hp('60%'), width: wp('90%'), backgroundColor: '#FEECA9', marginTop: hp('8%'), borderRadius: 10 } :
                            { height: hp('46%'), width: wp('90%'), backgroundColor: '#FEECA9', marginTop: hp('8%'), borderRadius: 10 }}>
                            <View style={values.type === "Select" ? { flexDirection: "row", marginTop: hp('7%'), marginBottom: hp('3%') } : { flexDirection: "row", marginTop: hp('7%') }}>
                                <Text style={{ color: 'black', fontSize: 15, marginLeft: wp('10%') }}>Titre:</Text>
                                <TextInput onChangeText={handleChange('title')} onBlur={handleBlur('title')}
                                    value={values.title} style={{ backgroundColor: 'white', width: wp('26%'), height: hp('3%'), borderColor: 'black', borderStyle: 'solid', borderWidth: 1, marginLeft: wp('2%') }}></TextInput>
                            </View>

                            {values.type === 'Select' &&
                                <View style={{ marginLeft: wp('10%') }}>
                                    <View style={{ flexDirection: "row", marginBottom: hp('1%') }}>
                                        <Text style={{ fontSize: 15 }}>Votre selecteur:</Text>
                                        <Picker style={{ height: hp('3%'), width: wp('45%') }} >
                                            {selector && selector.map((s, index) => {
                                                return (<Picker.Item label={s.name} value={s.name} key={index} />)
                                            })}
                                        </Picker>
                                    </View>
                                    <Text >Ajouter un champ à votre sélecteur:</Text>
                                    <View style={{ flexDirection: "row" }}>
                                        <TextInput style={{ backgroundColor: 'white', width: wp('40%') }} value={newSelectorInput} onChangeText={text => { setNewSelectorInput(text) }}></TextInput>
                                        <TouchableOpacity style={{ width: wp('30%'), backgroundColor: 'green', justifyContent: 'center' }} onPress={() => {
                                            if(newSelectorInput!== ""){
                                                putSelector(indicatorSelected.id_indicator, newSelectorInput).then(res => {
                                                     setNewSelectorInput("");
                                                });
                                            }
                                        }}>
                                            <Text style={{ textAlign: 'center', color: 'white' }}>
                                                Ajouter
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                    <Text >Supprimer un champ à votre sélecteur:</Text>
                                    <View style={{ flexDirection: "row" }}>
                                        <TextInput style={{ backgroundColor: 'white', width: wp('40%') }} value={deleteSelectorInput} onChangeText={text => { setDeleteSelectorInput(text) }}></TextInput>
                                        <TouchableOpacity style={{ width: wp('30%'), backgroundColor: 'red', justifyContent: 'center' }} onPress={() => {
                                            if(deleteSelectorInput !== ""){
                                                deleteSelectorValue(indicatorSelected.id_indicator, deleteSelectorInput).then(res => {
                                                     setDeleteSelectorInput("");
                                                });
                                            }
                                        }}>
                                            <Text style={{ textAlign: 'center', color: 'white' }}>
                                                Supprimer
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            }

                            <View style={values.type === "Select" ? { flexDirection: "row", marginTop: hp('3%') } : { flexDirection: "row", marginTop: hp('4%') }}>
                                <Text style={{ marginLeft: wp('10%') }}>Caché:</Text>
                                <Picker onValueChange={handleChange('hidden')} selectedValue={values.hidden} style={{ height: hp('3%'), width: wp('32%') }} >
                                    <Picker.Item label='Visible' value='Visible' />
                                    <Picker.Item label='Caché' value='Caché' />
                                </Picker>
                            </View>

                            <View style={{ flexDirection: "row", marginTop: hp('5%') }}>
                                <Text style={{ marginLeft: wp('10%') }}>Ordonnancement:</Text>
                                <TextInput onChangeText={handleChange('order')} onBlur={handleBlur('order')}
                                    value={values.order} style={{ backgroundColor: 'white', width: wp('30%'), height: hp('3%'), marginLeft: wp('1%'), borderColor: 'black', borderStyle: 'solid', borderWidth: 1 }}></TextInput>
                            </View>
                            {orderNotInt && <Text style={{color:'red', marginTop: hp('2%'), marginLeft:wp('10%')}}>L'ordonnancement doit être un nombre.</Text>}

                            <TouchableOpacity style={orderNotInt ?{ backgroundColor: 'black', marginTop: hp('5%') }: { backgroundColor: 'black', marginTop: hp('10%') }} onPress={() => handleSubmit()}>
                                <Text style={{ textAlign: 'right', marginRight: wp('7%'), color: 'white', marginBottom: 2, marginTop: 1 }}>Enregistrer</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ backgroundColor: '#CD5C5C', marginTop: 5 }} onPress={() => {
                                setBack(true);
                            }}>
                                <Text style={{ textAlign: 'right', marginRight: wp('10%'), color: 'white', marginBottom: 2, marginTop: 1 }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )}
            </Formik>

            <Modal animationType="fade" transparent={false} visible={modalIndicatorTitleVisible} >
                <View style={{ alignItems: 'center', height: hp('100%') }}>
                    <View style={{
                        backgroundColor: "#FFC9D3",
                        height: hp('30%'),
                        width: wp('89%'),
                        borderRadius: 5,
                        marginTop: hp('30%')
                    }}>
                        <View style={{ flexDirection: "row" }}>
                            <Text style={{ fontSize: 17, textAlignVertical: 'center', flex: 1, marginLeft: wp('2%') }}>Entrer le nom de l'indicateur à mettre à jour:</Text>
                            <TouchableHighlight onPress={() => {
                                setBack(true);
                            }} style={{ width: wp('15%'), height: hp('7%') }}>
                                <Text style={{ fontSize: 20, textAlign: 'center', textAlignVertical: 'center', flex: 1 }}>X</Text>
                            </TouchableHighlight>
                        </View>
                        <View style={{ flexDirection: "row", marginTop: hp('5%') }}>
                            <Text style={{ marginLeft: wp('2%'), fontSize: 15 }}>Titre de l'indicateur:</Text>
                            <TextInput style={{ height: hp('3%'), width: wp('30%'), backgroundColor: 'white', marginLeft: wp('2%') }} onChangeText={text => setIndicatorTitle(text)} value={indicatorTitle}></TextInput>
                        </View>
                        {indicatorDoesNotExist && <Text style={{color:'red', marginTop: hp('1%'), marginLeft: wp('2%')}}>Cet indicateur n'existe pas.</Text>}
                        <TouchableOpacity style={{ bottom: 0, position: 'absolute', width: wp('30%'), height: hp('10%'), right: 0, justifyContent: 'center', alignItems: "center" }} onPress={() => { checkIndicatorTitle() }} disabled={indicatorTitle !== null && indicatorTitle !== "" ? false : true}>
                            <TouchableHighlight style={{ backgroundColor: '#FF5468', width: wp('15%'), borderRadius: 5 }}>
                                <Text style={{ color: 'white' }}> Valider</Text>
                            </TouchableHighlight>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </View>
    );
}
export default UpdateIndicator;