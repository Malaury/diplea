import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { Redirect } from 'react-router-dom';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { deleteUser, getUserLoged } from '../database/user';
import instance from '../database/frontUtils';
import { getEspaces } from '../database/espace';
import { getIndicators } from '../database/indicateur';
import { getSelectors } from '../database/selector';
import { getData} from '../database/data';

const Profile = () => {
    const [goBack, setGoBack] = useState(false);
    const [redirectToHome, setRedirectToHome] = useState(false);
    const [espaces, setEspaces] = useState(null);
    const [indicators, setIndicators] = useState(null);
    const [user, setUser] = useState(null);
    const [selectors, setSelectors] = useState(null);
    const [data, setData] = useState(null);
    const [test] = useState(null);

    function sendDataAndDisconnection() {
        instance.post(`/data/data/${user}`, { data: data }, {}, {
        }).then(res => {
            instance.post(`/data/selector/${user}`, selectors ? { data: selectors }:{data:[]}, {}, {
            }).then(res => {
                instance.post(`/data/indicator/${user}`, indicators ? { data: indicators }:{data:[]}, {}, {
                }).then(res => {
                    instance.post(`/data/espace/${user}`, espaces ? { data: espaces }:{data:[]}, {}, {
                    }).then(res => {
                        deleteUser().then(res => setRedirectToHome(true));
                    })
                        .catch((error) => { console.log(error) });
                })
                    .catch((error) => { console.log(error) });
            })
                .catch((error) => { console.log(error) });
        })
            .catch((error) => { console.log(error) });
    }

    useEffect(() => {
        getUserLoged().then(data =>{
            console.log(data);
            setUser(data);
        })
        getEspaces().then(data => {
            setEspaces(data);
        });
        getIndicators().then(data => {
            setIndicators(data);
        });
        getSelectors().then(data => {
            setSelectors(data);
        });
        getData().then(data => {
            setData(data);
        });
    }, [test]);

    return (
        <View style={{ height: hp('100%') }}>
            {goBack === true && <Redirect to='/espace' />}
            {redirectToHome === true && <Redirect to='/' />}
            <TouchableOpacity disabled={true} style={{ backgroundColor: '#CD5C5C', height: hp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ color: 'white', fontSize: 20, position: 'absolute', bottom: 5 }}>Profile</Text>
            </TouchableOpacity>
            <View style={{ alignItems: 'center', marginTop: hp('10%') }}>
                <Image style={{
                    width: wp('60%'),
                    height: hp('30%')
                }} source={require("../assets/factory.png")}>
                </Image>
                <TouchableOpacity onPress={() => { sendDataAndDisconnection() }} style={{ backgroundColor: '#CD5C5C', height: hp('5%'), width: wp('30%'), alignItems: 'center', borderWidth: 1, borderColor: 'pink', marginTop: hp('3%') }}>
                    <Text style={{ color: 'white', fontSize: 15, position: 'absolute', bottom: 5 }}>Déconnexion</Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity style={{ width: wp('40%'), height: hp('20%'), bottom: hp('-5%'), position: 'absolute', justifyContent: 'center' }} onPress={() => setGoBack(true)}>
                <Image style={{
                    width: wp('15%'),
                    height: hp('7%'), marginLeft: wp('10%')
                }} source={require("../assets/left-arrow.png")} >
                </Image>
            </TouchableOpacity>
        </View>
    )
}
export default Profile;