import React, { useState, useEffect } from 'react';
import { View, Text, Picker } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { getDataByDateAndIndicator, updateData } from '../../database/data';

interface Props{
    pushData(id_indicator: string, data_text: string,tracker: boolean):any,
    title: string,
    id_indicator:number,
    data_date:string
}
const TrackerIndicator = ({pushData, title, id_indicator, data_date}: Props) =>{

    const [tracker, setTracker]= useState(false);
    const [data, setData] = useState(null);
    const [trackerPicker, setTrackerPicker] = useState("Invalider");

    useEffect(() => {
        getDataByDateAndIndicator(data_date,`${id_indicator}`).then(d => {
            if(d.length === 0){
                setTrackerPicker("Invalider");
                setData(null);
                return;
            }
            setData(d[0]);
            return;
        });
    }, [data_date]);

    useEffect(() => {
        if(!data) return;
        setTracker(data.tracker);
        if(data.tracker === "true"){
            setTrackerPicker("Valider");
        }else{
            setTrackerPicker("Invalider");
        }
    }, [data]);
 
    function sendTracker(t: string){
        if(t==="Valider"){
            setTrackerPicker("Valider");
            pushData(`${id_indicator}`,null, true);
        }
        else{
            setTrackerPicker("Invalider");
            pushData(`${id_indicator}`,null, false);
        }
    }

    function updateTracker(t: string){
        if(t==="Valider"){
            setTrackerPicker("Valider");
            updateData(`${id_indicator}`, null, true,data_date);
        }
        else{
            setTrackerPicker("Invalider");
            updateData(`${id_indicator}`, null, false,data_date);
        }
    }

    return(
        <View style={{ width:wp('80%'), backgroundColor:'#AADCF6', height:wp('20%')}}>
            <Text style={{fontSize:16, width: wp('80%'), marginTop:7, marginLeft:wp('5%')}}>{title}:</Text>
            <Picker selectedValue={trackerPicker} style={{width:wp('40%'), marginTop: hp('-1%'),marginLeft:wp('5%')}} onValueChange={t => {
                    if(!data){
                        sendTracker(t);
                    }
                    else{
                        updateTracker(t);
                    }
                }}>
                <Picker.Item label='Valider' value='Valider'></Picker.Item>
                <Picker.Item label='Invalider' value='Invalider'></Picker.Item>
            </Picker>
        </View>
    );
}
export default TrackerIndicator;