import React, { useState, useEffect } from 'react';
import { View, Text, TextInput } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { getDataByDateAndIndicator, updateData } from '../../database/data';

interface Props {
    pushData(id_indicator: string, data_text: string, tracker: boolean): any,
    title: string,
    id_indicator: string,
    data_date: string,
}
const TextIndicator = ({ pushData, title, id_indicator, data_date }: Props) => {

    const [text, setText] = useState(null);
    const [data, setData] = useState(null);

    useEffect(() => {
        getDataByDateAndIndicator(data_date, id_indicator).then(d => {
            if(d.length === 0){
                setText("");
                setData(null);
                return;
            }
            setData(d[0]);
            return;
        });
    }, [data_date]);

    useEffect(() => {
        if(!data) return;
        setText(data.data_text);
    }, [data]);

    function sendText(t: string) {
        setText(t);
        pushData(id_indicator, t, null);
    }

    function updateText(t: string){
        setText(t);
        updateData(id_indicator, t, null, data_date);
    }

    return (
        <View style={{ width: wp('80%'), backgroundColor: '#F6C9AA', height: hp('13%') }}>
            <Text style={{ fontSize: 16, width: wp('80%'), marginTop: 15, marginLeft: wp('5%') }}>{title}:</Text>
            <View style={{ alignItems: 'center' }}>
                <TextInput style={{ backgroundColor: 'white', width: wp('68%'), marginTop: hp('1%') }} value={text} onChangeText={t => {
                    if(!data){
                        sendText(t);
                    }else{
                        updateText(t);
                    }    
                }}></TextInput>
            </View>
        </View>
    );
}
export default TextIndicator;       