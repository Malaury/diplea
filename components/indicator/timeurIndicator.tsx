import React, { useState, useEffect } from 'react';
import { View, Text, Picker, TextInput } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { getDataByDateAndIndicator, updateData } from '../../database/data';

interface Props {
    pushData(id_indicator: string, data_text: string, tracker: boolean): any,
    title: string,
    id_indicator: number,
    data_date: string
}
const TimeurIndicator = ({ pushData, title, id_indicator, data_date }: Props) => {

    const [timeur, setTimeur] = useState(null);
    const [data, setData] = useState(null);
    const [timeurNotInt, setTimeurNotInt] = useState(null);

    useEffect(() => {
        getDataByDateAndIndicator(data_date, `${id_indicator}`).then(d => {
            if(d.length === 0){
                setTimeur("");
                setData(null);
                return;
            }
            setData(d[0]);
            return;
        });
    }, [data_date]);

    useEffect(() => {
        if (!data) return;
        setTimeur(data.data_text);
    }, [data]);

    function sendTimeur(t: string) {
        setTimeur(t);
        pushData(`${id_indicator}`, t, null);
    }

    function updateTimeur(t: string) {
        setTimeur(t);
        updateData(`${id_indicator}`, t, null, data_date);
    }
    return (
        <View style={{ width: wp('80%'), backgroundColor: '#F7DEDA'}}>
                <Text style={{ fontSize: 16, width: wp('90%'), marginTop: 15, marginLeft: wp('5%') }}>{title}:</Text>
                <TextInput style={timeurNotInt ? { marginLeft: wp('5%'),backgroundColor: 'white', width: wp('50%'), marginTop: hp('2%'), marginBottom: hp('1%') }: { marginLeft: wp('5%'),backgroundColor: 'white', width: wp('50%'), marginTop: hp('2%'), marginBottom: hp('2%') }} value={timeur} onChangeText={t => {
                    if (isNaN(parseInt(t)) && t!=="") {
                        setTimeurNotInt(true);
                    } else {
                        setTimeurNotInt(false);
                        if (!data) {
                            sendTimeur(t)
                        }
                        else{
                            updateTimeur(t);
                        }
                    }
                }}></TextInput>
            {timeurNotInt && <Text style={{color:'red', fontSize:10, marginBottom: hp('2%'),  marginLeft: wp('5%')}}>L'indicateur timeur ne supporte qu'une entrée de nombre.</Text>}
        </View>
    );
}
export default TimeurIndicator;