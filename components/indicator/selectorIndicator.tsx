import React, { useState, useEffect } from 'react';
import { View, Text, Picker, TouchableOpacity, TextInput, Switch, AsyncStorage } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { getDataByDateAndIndicator, updateData } from '../../database/data';
import { getSelectorsByIndicator } from '../../database/selector';

interface Props {
    pushData(id_indicator: string, data_text: string, tracker: boolean): any,
    title: string,
    id_indicator: number,
    data_date: string,
}
const SelectorIndicator = ({ pushData, title, id_indicator, data_date }: Props) => {

    const [selectorData, setSelectorData] = useState("Non selctionné");
    const [data, setData] = useState(null);
    const [selector, setSelector] = useState(null);

    useEffect(() => {
        getSelectorsByIndicator(`${id_indicator}`).then(d => {
            setSelector(d);
            if(d.length === 0){
                setData("Non selctionné");
                return;
            }
            getDataByDateAndIndicator(data_date, `${id_indicator}`).then(d => {
                if(d.length === 0 ){
                    setData("Non selctionné");
                    return;
                };
                setData(d[0]);
                return;
            });
        });
    }, [data_date]);

    useEffect(() => {
        if (!data) return;
        setSelectorData(data.data_text);
    }, [data]);


    function sendSelector(t: string) {
        setSelectorData(t);
        pushData(`${id_indicator}`, t, null).then()
    }

    function updateSelector(t: string) {
        setSelectorData(t);
        updateData(`${id_indicator}`, t, null, data_date);
    }

    return (
        <View style={{width: wp('80%'), backgroundColor: '#EAE4B2' }}>
            <Text style={{ fontSize: 16, width: wp('80%'), marginTop: 15, marginLeft: wp('5%') }}>{title}:</Text>
            <Picker selectedValue={selectorData} style={{ width: wp('50%'), marginLeft: wp('5%') }} onValueChange={t => {
                if (!data) {
                    sendSelector(t)
                }else{
                    updateSelector(t);
                }
            }}>
                <Picker.Item label={"Non selctionné"} value={"Non selctionné"}></Picker.Item>
                {selector && selector.map((s, index)  => {
                    return(<Picker.Item label={s.name} key={index} value={s.name}></Picker.Item>);
                })}
            </Picker>
        </View>
    );
}
export default SelectorIndicator;