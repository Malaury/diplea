import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, Text, Image, AsyncStorage, ScrollView, SafeAreaView } from "react-native";
import { Calendar, CalendarList, Agenda, LocaleConfig } from 'react-native-calendars';
import { Redirect } from 'react-router-dom';
import { getEspaceByName } from '../database/espace';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import SelectorIndicator from './indicator/selectorIndicator';
import TextIndicator from './indicator/textIndicator';
import TrackerIndicator from './indicator/trackerIndicator';
import TimeurIndicator from './indicator/timeurIndicator';
import { getIndicatorsByEspace } from '../database/indicateur';
import { putData } from '../database/data';
import moment from 'moment';

LocaleConfig.locales['fr'] = {
    monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
    monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
    today: 'Aujourd\'hui'
};
LocaleConfig.defaultLocale = 'fr';

const Espace = () => {

    const [redirectToProfile, setRedirectToProfile] = useState(false);
    const [redirectToConfig, setRedirectToConfig] = useState(false);
    const [redirectToGraphic, setRedirectToGraphic] = useState(false);
    const [espace, setEspace] = useState(null);
    const [redirectToHome, setRedirectToHome] = useState(false);
    const [indicators, setIndicators] = useState(null);
    const [currentDate, setCurrentDate] = useState(moment()
        .utcOffset('+01:00')
        .format('YYYY-MM-DD'));
    const [dateLimited] = useState(moment()
        .utcOffset('+01:00')
        .format('YYYY-MM-DD'));

    async function pushData(id_indicator: string, data_text: string, tracker: boolean) {
        await putData(id_indicator, data_text, currentDate, tracker);
        return true;
    }

    useEffect(() => {
        AsyncStorage.getItem('name').then(data => {
            getEspaceByName(data).then(esp => {
                setEspace(esp[0]);
            });
        });
        AsyncStorage.getItem('id_espace').then(id_espace => {
             getIndicatorsByEspace(id_espace).then(data => {
                setIndicators(data);
            });
        })
           
    }, []);

    return (
        <View style={{ height: hp('100%') }}>
            {redirectToConfig === true && <Redirect to='/configuration' />}
            {redirectToHome === true && <Redirect to='/' />}
            {redirectToGraphic === true && <Redirect to='/graphic' />}
            {redirectToProfile === true && <Redirect to='/profile' />}
            <View style={{ flexDirection: "row", backgroundColor: '#CD5C5C', height: hp('11%') }}>
                <Text style={{ fontSize: 25, textAlign: 'center', width: wp('100%'), color: 'white', position: 'absolute', bottom: 0, marginBottom: hp('0.7%') }}> {espace ? espace.name : null} </Text>
                <TouchableOpacity style={{height: hp('14%'),width: wp('25%')}} onPress={() => { setRedirectToProfile(true)}}>
                    <Image style={{
                        width: wp('6%'),
                        height: hp('3.5%'),
                        position: 'absolute',
                        bottom: 0,
                        marginBottom: hp('4.5%'),
                        marginLeft: wp('5%')
                    }} source={require("../assets/user.png")}>
                    </Image>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: wp('20%'), height: hp('13%'), position: 'absolute', right: 0, bottom: hp('-3%') }} onPress={() => setRedirectToConfig(true)}>
                    <Image style={{
                        width: wp('7%'),
                        height: hp('4%'),
                        position: 'absolute',
                        bottom: hp('4%'),
                        right: 0,
                        marginRight: wp('5%')
                    }} source={require("../assets/config.png")}></Image>
                </TouchableOpacity>
            </View>
            <View style={{ alignItems: 'center', marginTop: hp('1%') }}>
                <Calendar markedDates={{ [currentDate]: { selected: true } }} style={{ width: wp('80%'), height: hp('50%') }} minDate={'2020-01-01'} maxDate={dateLimited}
                    onDayPress={(day) => {
                        setCurrentDate(moment(day.dateString).format('YYYY-MM-DD'));
                    }}></Calendar>
            </View>

            {/*Indicators*/}
            <View style={{ height: hp('27%'), marginTop: hp('-15%') }}>
                <View style={{ top: hp('20%'), alignItems: 'center', flex: 1 }}>
                    <ScrollView>
                        {indicators && indicators.map((e, index) => {
                            switch (e.type) {
                                case 'Text':
                                    return (
                                        <TextIndicator data_date={currentDate} key={index} pushData={pushData} title={e.title} id_indicator={e.id_indicator}></TextIndicator>
                                    );
                                    break;
                                case 'Tracker':
                                    return (
                                        <TrackerIndicator data_date={currentDate} key={index} pushData={pushData} title={e.title} id_indicator={e.id_indicator}></TrackerIndicator>
                                    );
                                    break;
                                case 'Select':
                                    return (
                                        <SelectorIndicator data_date={currentDate} key={index} pushData={pushData} title={e.title} id_indicator={e.id_indicator}></SelectorIndicator>
                                    );
                                    break;
                                case 'Timeur':
                                    return (
                                        <TimeurIndicator data_date={currentDate} key={index} pushData={pushData} title={e.title} id_indicator={e.id_indicator}></TimeurIndicator>
                                    );
                                    break;
                            }
                        })}
                    </ScrollView>
                </View>
            </View>

            <View style={{ flexDirection: "row", position: 'absolute', bottom: hp('-5%'), width: wp('100%'), alignItems: 'center' }}>
                <TouchableOpacity style={{ width: wp('22%'), height: hp('10%'), alignItems: 'center' }} onPress={() => { setRedirectToHome(true) }}>
                    <Image style={{
                        width: wp('9%'),
                        height: hp('5%'),
                        bottom: 0,
                        position: 'absolute',
                        marginBottom: hp('3%')
                    }} source={require("../assets/left-arrow.png")} ></Image>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: wp('22%'), height: hp('10%'), right: 0, position: 'absolute', alignItems: 'center', justifyContent: 'center' }} onPress={() => { setRedirectToGraphic(true) }}>
                    <Image style={{
                        width: wp('9%'),
                        height: hp('5%'),
                        marginTop: hp('-2%')
                    }} source={require("../assets/statistics.png")} ></Image>
                </TouchableOpacity>
            </View>
        </View>
    );
}
export default Espace;