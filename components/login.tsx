import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Redirect } from 'react-router-dom';
import { Formik } from 'formik';
import instance from '../database/frontUtils';
import { putUser } from '../database/user';
import { truncateData, putDataFromDB } from '../database/data';
import { truncateIndicator,  putIndicatorFromDB } from '../database/indicateur';
import { truncateSelector,  putSelectorFromDB } from '../database/selector';
import { truncateEspace, putEspaceFromDB } from '../database/espace';
const Login = () => {
    const [redirectToRegister, setRedirectToRegister] = useState(false);
    const [usernomeNotExists, setUsernameNotExists] = useState(false);
    const [redirectToHome, setRedirectToHome] = useState(false);
    const [espaces, setEspaces] = useState(null);
    const [indicators, setIndicators] = useState(null);
    const [selectors, setSelectors] = useState(null);
    const [data, setData] = useState(null);

    useEffect(() => {
        if (!espaces) return;
        truncateEspace().then(res => {
            espaces.map(espace => {
                putEspaceFromDB(espace.name,espace.id_espace);
            });
        })
    }, [espaces]);

    useEffect(() => {
        if (!indicators) return;
        truncateIndicator().then(res => {
            indicators.map(indicator => {
                putIndicatorFromDB(indicator.id_espace, indicator.title, indicator.type, indicator.display_order, indicator.hidden, indicator.id_indicator);
            });
        })
    }, [indicators]);

    useEffect(() => {
        if (!selectors) return;
        truncateSelector().then(res => {
            selectors.map(selector => {
                putSelectorFromDB(selector.id_indicator, selector.name, selector.id_selector);
            });
        });
    }, [selectors]);

    useEffect(() => {
        if (!data) return;
        truncateData().then(res => {
            data.map(d => {
                putDataFromDB(d.id_indicator, d.data_text, d.data_date, d.tracker, d.id_data);
            })
        })
    }, [data]);

    function getDataFromDataExt(id_user: number) {
        return new Promise((resolve) => {
            instance.get(`/data/espace/${id_user}`, {
            }).then(resEspace => {
                setEspaces(resEspace.data);
                instance.get(`/data/indicator/${id_user}`, {
                }).then(resIndicator => {
                    setIndicators(resIndicator.data);
                    instance.get(`/data/data/${id_user}`, {
                    }).then(resData => {
                        setData(resData.data);
                        instance.get(`/data/selector/${id_user}`, {
                        }).then(resSelector => {
                            setSelectors(resSelector.data);
                            setTimeout(function () { //Start the timer
                                resolve(0);//After 1 second, set render to true
                            }.bind(this), 1000)
                        })
                            .catch((error) => { console.log(error) });
                    })
                        .catch((error) => { console.log(error) });
                })
                    .catch((error) => { console.log(error) });
            })
                .catch((error) => { console.log(error) });
        })
    }

    return (
        <View style={{ height: hp('100%') }}>
            {redirectToRegister && <Redirect to='/register' />}
            {redirectToHome && <Redirect to='/' />}
            <TouchableOpacity disabled={true} style={{ backgroundColor: '#CD5C5C', height: hp('10%'), alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ color: 'white', fontSize: 20, position: 'absolute', bottom: 5 }}>Login</Text>
            </TouchableOpacity>
            <Formik
                initialValues={{
                    'username': '',
                    'password': ''
                }}
                onSubmit={values => {
                    if (values.username === "" || values.password === "") return;
                    instance.post(`/user/check`, { 'username': values.username, 'password': values.password }, {}, {
                    }).then((res) => {
                        if (res.data.length !== 0) {
                            console.log(res.data[0].id_user)
                            getDataFromDataExt(res.data[0].id_user).then(r => {
                                putUser(res.data[0].id_user).then(res =>  setRedirectToHome(true))
                            })
                            return;
                        }
                        else {
                            setUsernameNotExists(true);
                        }
                    })
                        .catch((error) => { console.log(error) });
                }}>
                {({ handleChange, handleSubmit, values }) => (
                    <View style={{ alignItems: 'center' }}>
                        <View style={{ height: hp('25%'), width: wp('90%'), backgroundColor: '#FEEBBD', marginTop: hp('7%'), borderRadius: 10 }}>
                            <View style={{ flexDirection: 'row', marginTop: hp('4%'), marginLeft: wp('15%') }}>
                                <Text style={{ fontSize: 18 }}>Pseudo: </Text>
                                <TextInput onChangeText={handleChange('username')} value={values.username} style={{ height: hp('3%'), width: wp('30%'), backgroundColor: 'white', marginTop: 4, marginLeft: wp('2%'), borderWidth: 1 }}></TextInput>
                            </View>
                            {usernomeNotExists && <Text style={{ marginLeft: wp('15%'), marginTop: hp('1%') }}>Pseudo ou mot de passe incorrect.</Text>}
                            <View style={usernomeNotExists ? { flexDirection: 'row', marginTop: hp('1%'), marginLeft: wp('15%') } : { flexDirection: 'row', marginTop: hp('3%'), marginLeft: wp('15%') }}>
                                <Text style={{ fontSize: 18 }}>Mot de passe: </Text>
                                <TextInput onChangeText={handleChange('password')} secureTextEntry={true} value={values.password} style={{ height: hp('3%'), width: wp('30%'), backgroundColor: 'white', marginTop: 4, marginLeft: wp('2%'), borderWidth: 1 }}></TextInput>
                            </View>
                            <View>
                                <TouchableOpacity onPress={() => { handleSubmit() }} style={usernomeNotExists ? { backgroundColor: '#CD5C5C', width: wp('31%'), height: hp('4%'), right: 0, marginTop: hp('3%'), marginLeft: ('61%'), justifyContent: 'center' } : { backgroundColor: '#CD5C5C', width: wp('31%'), height: hp('4%'), right: 0, marginTop: hp('5%'), marginLeft: ('61%'), justifyContent: 'center' }}>
                                    <Text style={{ color: 'white', textAlign: 'center' }}>S'enregistrer</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                )}
            </Formik>
            <View style={{ alignItems: 'center' }}>
                <Text style={{ marginTop: hp('2%'), textAlign: 'center' }}> Vous n'avez pas encore de compte ? </Text>
                <TouchableOpacity style={{ backgroundColor: '#E88F82', width: wp('26%'), height: hp('4%'), marginTop: hp('2%'), justifyContent: 'center' }} onPress={() => { setRedirectToRegister(true) }}>
                    <Text style={{ color: 'white', textAlign: 'center' }}>S'enregistrer</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}
export default Login;