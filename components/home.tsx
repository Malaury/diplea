import React, { useState, useEffect } from "react";
import { View, AsyncStorage, StyleSheet, Image, Picker, TouchableOpacity, Text, Modal, TouchableHighlight, TextInput } from "react-native";
import { getEspaces, putEspace, getEspaceByName, deleteEspaceByName } from '../database/espace';
import { Redirect } from 'react-router-dom';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { getUserLoged } from '../database/user';
import  {parseData} from '../database/utils';

const Home = () => {

    const [createSpaceModalVisible, setCreateSapceModalVisible] = useState(false);
    const [deleteSpaceModalVisible, setDeleteSapceModalVisible] = useState(false);
    const [spaceTitle, setSpaceTitle] = useState(null);
    const [espaces, setEspaces] = useState(null);
    const [redirect, setRedirect] = useState(false);
    const [espaceSelected, setEspaceSelected] = useState(null);
    const [espaceInSession, setEspaceInSession] = useState(null);
    const [titleNotUniq, setTitleNotUniq] = useState(false);
    const [titleNotExist, setTitleNotExist] = useState(false);
    const [redirectToLogin, setRedirectToLogin] = useState(false);

    useEffect(() => {
        getUserLoged().then(result => {
            if (result === null) {
                setRedirectToLogin(true);
                return;
            }
            else{
                getEspaces().then(d => {
                    const data = parseData(d);
                    setEspaces(data);
                    if (data) {
                        setEspaceSelected(data[0].name);
                        setEspaceInSession(data[0].name);
                    }
                });
            }
        }
        );
    }, [deleteSpaceModalVisible, createSpaceModalVisible]);


    useEffect(() => {
        if (espaceInSession) {
            console.log('Update espace id Session storage: ' + espaceInSession);
            AsyncStorage.setItem('name', espaceInSession);
        }
        getEspaceByName(espaceInSession).then(espace =>{
            if(espace.length === 0) return;
            AsyncStorage.setItem('id_espace', espace[0].id_espace);
        });
    }, [espaceInSession]);

    function updateEspaceSelected(espaceName: string) {
        setEspaceInSession(espaceName);
    }

    const style = StyleSheet.create({
        logo: {
            marginTop: hp('6%'),
            width: wp('75%'),
            height: hp('10%'),
            marginLeft: wp('23%'),
            marginBottom: hp('7%')
        },
        mainImage: {
            width: wp('70%'),
            height: hp('33%'),
            marginLeft: wp('23%')
        },
        buttonEnterInSpace: {
            marginTop: hp('1%'),
            backgroundColor: "#7B65AE",
        },
        textInSpace: {
            marginTop: hp('0.5%'),
            marginBottom: hp('0.5%'),
            color: "white",
            marginLeft: wp('25%')
        },
        buttonDeleteSpace: {
            marginTop: hp('6%'),
            backgroundColor: "#E88F82",
        },
        buttonCreateSpace: {
            backgroundColor: "#59A4FF",
        },
        blackLine: {
            backgroundColor: "black",
            marginTop: hp('6%'),
            height: hp('4%')
        },
        leftLine: {
            backgroundColor: "#7B65AE",
            height: hp('110%'),
            width: wp('7%'),
            position: 'absolute',
            marginLeft: wp('7%')
        },
        modal: {
            backgroundColor: "#FFC9D3",
            height: hp('30%'),
            width: wp('89%'),
            borderRadius: 5,
            marginTop: hp('30%')
        }
    });

    return (
        <View>
            {redirect === true && <Redirect to='/espace' />}
            {redirectToLogin === true && <Redirect to='/login' />}
            <TouchableOpacity disabled={true} style={style.blackLine} />
            <Image style={style.logo} source={require("../assets/phrase.png")} ></Image>
            <Image style={style.mainImage} source={require("../assets/image_fille.png")} ></Image>
            <Picker selectedValue={espaceSelected ? espaceSelected : null} style={{ height: hp('14%'), width: wp('40%'), marginLeft: wp('24%') }} onValueChange={(value) => {
                setEspaceSelected(value);
                updateEspaceSelected(value);
            }}>
                {espaces ? espaces.map((e, index) => {
                    return (
                        <Picker.Item color="#7B65AE" key={index} label={e.name} value={e.name} />);
                }) : null}
            </Picker>
            <TouchableOpacity style={style.buttonEnterInSpace} onPress={() => setRedirect(true)}>
                <Text style={style.textInSpace}> ENTRER DANS CET ESPACE </Text>
            </TouchableOpacity>

            <TouchableOpacity style={style.buttonCreateSpace} onPress={() => {
                setCreateSapceModalVisible(!createSpaceModalVisible);
            }}>
                <Text style={style.textInSpace}> CREER UN ESPACE </Text>
            </TouchableOpacity>
            <TouchableOpacity style={style.buttonDeleteSpace} onPress={() => {
                setDeleteSapceModalVisible(!deleteSpaceModalVisible);
            }}>
                <Text style={style.textInSpace}> SUPPRIMER UN ESPACE </Text>
            </TouchableOpacity>
            <TouchableOpacity disabled={true} style={style.leftLine}>
            </TouchableOpacity>

            {/* Modal create space */}
            <Modal animationType="fade" transparent={false} visible={createSpaceModalVisible} >
                <View style={{ alignItems: 'center', height: hp('100%') }}>
                    <View style={style.modal}>
                        <View style={{ flexDirection: "row" }}>
                            <Text style={{ fontSize: 17, textAlignVertical: 'center', flex: 1, marginLeft: wp('2%') }}>Creation d'un nouvel espace</Text>
                            <TouchableHighlight onPress={() => {
                                setCreateSapceModalVisible(!createSpaceModalVisible);
                            }} style={{ width: wp('15%'), height: hp('7%') }}>
                                <Text style={{ fontSize: 20, textAlign: 'center', textAlignVertical: 'center', flex: 1 }}>X</Text>
                            </TouchableHighlight>
                        </View>
                        <View style={{ flexDirection: "row", marginTop: hp('5%') }}>
                            <Text style={{ marginLeft: wp('2%'), fontSize: 15 }}>Titre de l'espace:</Text>
                            <TextInput style={{ height: hp('3%'), width: wp('30%'), backgroundColor: 'white', marginLeft: wp('2%') }} onChangeText={text => setSpaceTitle(text)} value={spaceTitle}></TextInput>
                        </View>
                        {titleNotUniq && <Text style={{ color: 'red', marginLeft: wp('2%'), marginTop: hp('3%') }}>Ce nom d'espace est déjà prit.</Text>}
                        <TouchableOpacity style={{ bottom: 0, position: 'absolute', width: wp('20%'), height: hp('6%'), right: 0 }} onPress={() => {
                            putEspace(spaceTitle).then((enter) => {
                                if (enter === true) {
                                    setCreateSapceModalVisible(false);
                                    setSpaceTitle("");
                                    setTitleNotUniq(false);
                                }
                                else {
                                    setTitleNotUniq(true);
                                }
                            })
                        }} disabled={spaceTitle !== null && spaceTitle !== "" ? false : true}>
                            <TouchableHighlight style={{ backgroundColor: '#FF5468', width: wp('15%'), borderRadius: 5 }}>
                                <Text style={{ color: 'white' }}> Valider</Text>
                            </TouchableHighlight>
                        </TouchableOpacity>
                    </View>
                    <Image style={{
                        width: wp('50%'),
                        height: hp('30%'),
                        bottom: 0,
                        position: 'absolute',
                        marginBottom: hp('10%')
                    }} source={require("../assets/createSpace.png")} ></Image>
                </View>
            </Modal>

            <Modal animationType="fade" transparent={false} visible={deleteSpaceModalVisible} >
                <View style={{ alignItems: 'center', height: hp('100%') }}>
                    <View style={style.modal}>
                        <View style={{ flexDirection: "row" }}>
                            <Text style={{ fontSize: 17, textAlignVertical: 'center', flex: 1, marginLeft: wp('2%') }}>Supression d'un espace</Text>
                            <TouchableHighlight onPress={() => {
                                setDeleteSapceModalVisible(!deleteSpaceModalVisible);
                            }} style={{ width: wp('15%'), height: hp('7%') }}>
                                <Text style={{ fontSize: 20, textAlign: 'center', textAlignVertical: 'center', flex: 1 }}>X</Text>
                            </TouchableHighlight>
                        </View>
                        <View style={{ flexDirection: "row", marginTop: hp('5%') }}>
                            <Text style={{ marginLeft: wp('2%'), fontSize: 15 }}>Titre de l'espace:</Text>
                            <TextInput style={{ height: hp('3%'), width: wp('30%'), backgroundColor: 'white', marginLeft: wp('2%') }} onChangeText={text => setSpaceTitle(text)} value={spaceTitle}></TextInput>
                        </View>
                        {titleNotExist && <Text style={{ marginLeft: wp('2%'), color: 'red', marginTop: hp('1%') }}>Cet espace n'existe pas.</Text>}
                        <TouchableOpacity style={{ bottom: 0, position: 'absolute', width: wp('20%'), height: hp('6%'), right: 0 }} onPress={() => {
                            deleteEspaceByName(spaceTitle).then((result: boolean) => {
                                setTitleNotExist(!result);
                                if (result === true) {
                                    setDeleteSapceModalVisible(false);
                                    setSpaceTitle("");
                                }
                            })
                        }}
                            disabled={spaceTitle !== null && spaceTitle !== "" ? false : true}>
                            <TouchableHighlight style={{ backgroundColor: '#FF5468', width: wp('15%'), borderRadius: 5 }}>
                                <Text style={{ color: 'white' }}> Valider</Text>
                            </TouchableHighlight>
                        </TouchableOpacity>
                    </View>
                    <Image style={{
                        width: wp('70%'),
                        height: hp('30%'),
                        bottom: 0,
                        position: 'absolute',
                        marginBottom: hp('10%')
                    }} source={require("../assets/deleteSpace.jpg")} ></Image>
                </View>
            </Modal>
        </View>
    );
}
export default Home;