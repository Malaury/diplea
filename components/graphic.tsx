import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Redirect } from 'react-router-dom';

const Graphic = () => {

    const [goBack, setGoBack] = useState(false);

    return (
        <View> 
            {goBack === true && <Redirect to='/espace'/>}
            <TouchableOpacity disabled={true} style={{ backgroundColor: '#CD5C5C', height: hp('10%'), alignItems: 'center' }}>
                <Text style={{ color: 'white', fontSize: 20, position: 'absolute', bottom: 5 }}>Graphiques</Text>
            </TouchableOpacity>
        </View>
    );
}
export default Graphic;