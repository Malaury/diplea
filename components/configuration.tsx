import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, AsyncStorage, Modal, TouchableHighlight, TextInput } from 'react-native';
import { Redirect } from 'react-router-dom';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { getAllIndicatorsByEspace, deleteIndicatorByTitleAndEspace } from '../database/indicateur';
import { isEmpty, isNil } from 'lodash';

const Configuration = () => {

    const [redirectToCreateIndicator, setRedirectToCreateIndicator] = useState(false);
    const [redirectToEspace, setRedirectToEspace] = useState(false);
    const [redirectToUpdateIndicator, setRedirectToUpdateIndicator] = useState(false);
    const [indicators, setIndicators] = useState(null);
    const [dataIndicatorsDisplay, setDataIndicatorsDisplay] = useState(null);
    const [deleteIndicatorVisible, setDeleteIndicatorVisible] = useState(false);
    const [indicatorTitle, setIndicatorTitle] = useState("");
    const [indicatorTitleDoesNotExist, setIndicatorTitleDoesNotExist]= useState(false);

    useEffect(() => {
        let dataIndicators: any = [];
        indicators && !isEmpty(indicators) &&
            indicators.forEach(indicator => {
                let dataIndicator = [];
                dataIndicator = [indicator.title, indicator.type, indicator.hidden];
                dataIndicators.push(dataIndicator);
            });
        setDataIndicatorsDisplay(dataIndicators);
    }, [indicators]);

    useEffect(() => {
        AsyncStorage.getItem('id_espace').then(id => {
            getAllIndicatorsByEspace(id).then(data => {
                setIndicators(data);
            })
        });
    }, []);

    useEffect(() => {
        AsyncStorage.getItem('id_espace').then(id => {
            getAllIndicatorsByEspace(id).then(data => {
                setIndicators(data);
            })
        });
    }, [deleteIndicatorVisible]);

    return (
        <View style={{ height: hp('100%') }}>
            {redirectToCreateIndicator === true && <Redirect to='/create/indicator' />}
            {redirectToEspace === true && <Redirect to='/espace' />}
            {redirectToUpdateIndicator === true && <Redirect to='/update/indicator' />}
            <TouchableOpacity style={{ backgroundColor: '#CD5C5C', height: hp('11%') }}></TouchableOpacity>
            <Text style={{ textAlign: 'center', marginTop: hp('-5%'), fontSize: 18, color: 'white' }}>Configuration des indicateurs</Text>

            <View style={{ alignItems: 'center', marginTop: hp('5%') }}>
                <Table style={{ width: wp('80%') }} borderStyle={{ borderWidth: 2 }}>
                    <Row style={{ backgroundColor: '#F9DA7F' }} textStyle={{ textAlign: 'center' }} data={[`Titre de l'indicateur`, 'Type', 'Caché']} />
                    <Rows textStyle={{ textAlign: 'center' }} data={dataIndicatorsDisplay}></Rows>
                </Table>
            </View>

            <View style={{ width: wp('100%'), flexDirection: "row", position: 'absolute', bottom: 0 }}>
                <TouchableOpacity style={{ width: wp('40%'), height: hp('20%'), bottom: hp('-5%') }} onPress={() => setRedirectToEspace(true)}>
                    <Image style={{
                        width: wp('15%'),
                        height: hp('7%'), marginLeft: wp('10%'), bottom: hp('5%'), position: 'absolute'
                    }} source={require("../assets/left-arrow.png")} ></Image>
                </TouchableOpacity>
                <View style={{ right: 0, position: 'absolute', bottom: 0, width: wp('50%') }}>
                    <TouchableOpacity style={{ backgroundColor: '#81C178', borderWidth: 1, borderRightWidth: 0 }} onPress={() => setRedirectToCreateIndicator(true)}>
                        <Text style={{ textAlign: 'center', marginRight: wp('2%'), marginLeft: wp('1%') }}>Créer un nouvel indicateur</Text>
                    </TouchableOpacity>
                    <View style={{ height: hp('2%') }}></View>
                    <TouchableOpacity style={{ backgroundColor: '#F9DA7F', borderWidth: 1, borderRightWidth: 0 }} onPress={() => { setRedirectToUpdateIndicator(true)}}>
                        <Text style={{ textAlign: 'center', marginRight: wp('2%'), marginLeft: wp('1%') }}>Mettre à jour un indicateur</Text>
                    </TouchableOpacity>
                    <View style={{ height: hp('2%') }}></View>
                    <TouchableOpacity style={{ backgroundColor: '#E1AC9F', borderWidth: 1, borderRightWidth: 0 }} onPress={() => setDeleteIndicatorVisible(true)}>
                        <Text style={{ textAlign: 'center', marginRight: wp('2%'), marginLeft: wp('1%') }}>Supprimer un indicateur</Text>
                    </TouchableOpacity>
                </View>
            </View>

            {/**Modal delete indicator */}
            <Modal animationType="fade" transparent={false} visible={deleteIndicatorVisible} >
                <View style={{ alignItems: 'center', height: hp('100%') }}>
                    <View style={{
                        backgroundColor: "#FFC9D3",
                        height: hp('30%'),
                        width: wp('89%'),
                        borderRadius: 5,
                        marginTop: hp('30%')
                    }}>
                        <View style={{ flexDirection: "row" }}>
                            <Text style={{ fontSize: 17, textAlignVertical: 'center', flex: 1, marginLeft: wp('2%') }}>Supression d'un indicateur</Text>
                            <TouchableHighlight onPress={() => {
                                setDeleteIndicatorVisible(!deleteIndicatorVisible);
                            }} style={{ width: wp('15%'), height: hp('7%') }}>
                                <Text style={{ fontSize: 20, textAlign: 'center', textAlignVertical: 'center', flex: 1 }}>X</Text>
                            </TouchableHighlight>
                        </View>
                        <View style={{ flexDirection: "row", marginTop: hp('5%') }}>
                            <Text style={{ marginLeft: wp('2%'), fontSize: 15 }}>Titre de l'indicateur:</Text>
                            <TextInput style={{ height: hp('3%'), width: wp('30%'), backgroundColor: 'white', marginLeft: wp('2%') }} onChangeText={text => setIndicatorTitle(text)} value={indicatorTitle}
                            ></TextInput>
                        </View>
                        {indicatorTitleDoesNotExist && <Text style={{color:'red', marginTop:hp('1%'), marginLeft:wp('2%')}}>Cet indicateur n'existe pas.</Text>}
                        <TouchableOpacity style={{ bottom: 0, position: 'absolute', width: wp('20%'), height: hp('6%'), right: 0 }} onPress={() => { AsyncStorage.getItem('id_espace').then(id => {deleteIndicatorByTitleAndEspace(indicatorTitle,id).then((result : boolean) => {
                            setIndicatorTitleDoesNotExist(!result);
                            if(result === true){
                                setDeleteIndicatorVisible(false); 
                                setIndicatorTitle("");
                            }
                        }) })}} disabled={indicatorTitle !== null && indicatorTitle !== "" ? false : true}>
                            <TouchableHighlight style={{ backgroundColor: '#FF5468', width: wp('15%'), borderRadius: 5 }}>
                                <Text style={{ color: 'white' }}> Valider</Text>
                            </TouchableHighlight>
                        </TouchableOpacity>
                    </View>
                    <Image style={{
                        width: wp('70%'),
                        height: hp('30%'),
                        bottom: 0,
                        position: 'absolute',
                        marginBottom: hp('10%')
                    }} source={require("../assets/deleteSpace.jpg")} ></Image>
                </View>
            </Modal>
        </View>
    )
}
export default Configuration;